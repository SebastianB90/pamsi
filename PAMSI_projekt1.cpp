#include "Modul.hh"

using namespace std;

//Pakiet ma 10 znaków
#define PAKIET 10

void losowanie(Priority_queue &W_JANA, Priority_queue &W_ANNY)
{
    int n;
    srand(time(NULL));
    while (!W_JANA.isEmpty())
    {
        n = rand() % W_JANA.size();
        message W = W_JANA.remove(n);
        W_ANNY.insert(n, W.text);
    }
}

int main()
{
    string Inwokacja = "Litwo! Ojczyzno moja! Ty jestes jak zdrowie, Ile cie trzeba cenic, ten tylko sie dowie, Kto  cie  stracil. Dzis pieknosc twa w  calej  ozdobie  Widze i opisuje, bo tesknie po tobie"; //Wiadomosc W
    Priority_queue W_JANA;                                                                                                                                                                                //Wiadomosc W, ktora Jan wyslal do Anny
    Priority_queue W_ANNY;                                                                                                                                                                                //Wiadomosc W, ktora Anna otrzymala od Jana. Elementy w tej kolejce to pakiety po 10 znakow, w losowej kolejnosci
    int pakiety;

    //Dzielimy wiadomosc W na pakiety, czyli dlugosc wiadomosc dzielimy na liczbe znakow przypadajaca na jeden pakiet i rzutujemy na double
    pakiety = (double)Inwokacja.length() / PAKIET;

    cout << "Liczba pakietow wynosi: ";
    cout << pakiety << endl;

    for (int i = 0; i < pakiety; i++)
    {
        W_JANA.insert(i, Inwokacja.substr(i * PAKIET, PAKIET));
    }

    // Teraz Anna otrzymuje wiadomosc w n pakietach w losowej kolejnosci

    string final_message;

    cout << "1. Wiadomosc, ktora Jan wyslal Annie" << endl << endl;
    W_JANA.print();
    cout << endl;
    W_JANA.merge(final_message);

    losowanie(W_JANA, W_ANNY);
    cout << "2. Wiadomosc w losowej kolejnosci" << endl << endl;
    W_ANNY.print();
    cout << endl;
    cout << "3. Wiadomosc, ktora dostala Anna :)" << endl << endl;
    cout << final_message << endl;
    return 0;
}