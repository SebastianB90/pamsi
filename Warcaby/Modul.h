#ifndef MODUL_H
#define MODUL_H

#include <iostream>
#include <stdlib.h>

struct pozycja
{
    int x, y;
};

struct Ruch
{
    struct pozycja przed;
    struct pozycja po;
    struct pozycja bicie;
    struct pozycja krolowa;
};

class Plansza
{
private:
    int wiersze;
    int kolumny;

public:
    char **pola;
    char kolej = 'b'; // czyja jest kolej?

    Plansza(int wiersze, int kolumny);
    void Ustaw_pionki_na_planszy();
    void Wyswietl_plansze();
    void Ruch(/*struct pozycja przed, struct pozycja po, struct pozycja bicie, struct pozycja krolowa*/ struct Ruch ruch);

    bool Czy_puste(int x, int y);
    bool Czy_przeciwnik(int x, int y);
    bool Czy_na_planszy(int x, int y);
    void kolej_przeciwnika();
    int Ocena(char gracz);
};

// PORUSZANIE SIE

struct element_listy
{
    struct Ruch ruch;
    struct element_listy *next;
};

class Ruchy
{
private:
    struct element_listy *posuniecia;
    Plansza *plansza;

public:
    Ruchy(Plansza *plansza)
    {
        this->plansza = plansza;
        posuniecia = NULL;
        // sprawdzamy kazde pole, jesli jest tam nasz pionek to
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                // pole nie jest puste i nie ma przeciwnika, czyi jest nasz pionek
                if (((plansza->Czy_puste(i, j)) == 0) && ((plansza->Czy_przeciwnik(i, j)) == 0))
                {
                    posuniecia = Ruch_pionkiem(i, j, posuniecia, plansza);
                }
            }
        }
    }
    struct element_listy *Poczatek();
    struct element_listy *Dodaj_posuniecie(struct pozycja przed, struct pozycja po, struct element_listy *posuniecia, Plansza *plansza);
    struct pozycja pozycja(int x, int y);
    struct element_listy *Ruch_pionkiem(int x, int y, struct element_listy *posuniecia, Plansza *plansza);
    struct element_listy *Ruch_krolowa(int x, int y, struct element_listy *posuniecia, Plansza *plansza);
    struct element_listy *MozliwyRuch(struct pozycja poczatek, struct pozycja koniec);
};

#endif