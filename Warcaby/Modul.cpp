#include "Modul.h"
#include <iostream>

using namespace std;

Plansza::Plansza(int wiersze, int kolumny)
{
    this->wiersze = wiersze;
    this->kolumny = kolumny;
    pola = new char *[wiersze];
    for (int i = 0; i < wiersze; i++)
    {
        pola[i] = new char[kolumny];
    }
}

void Plansza::Ustaw_pionki_na_planszy()
{
    // puste pola
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            pola[i][j] = ' ';
        }
    }
    // biale
    for (int i = 0; i < 8; i = i + 2)
    {
        pola[0][i] = 'b';
    }
    for (int i = 1; i < 8; i = i + 2)
    {
        pola[1][i] = 'b';
    }
    for (int i = 0; i < 8; i = i + 2)
    {
        pola[2][i] = 'b';
    }
    // czarne
    for (int i = 1; i < 8; i = i + 2)
    {
        pola[5][i] = 'c';
    }
    for (int i = 0; i < 8; i = i + 2)
    {
        pola[6][i] = 'c';
    }
    for (int i = 1; i < 8; i = i + 2)
    {
        pola[7][i] = 'c';
    }
}

void Plansza::Wyswietl_plansze()
{
    int k = 8;
    for (int i = 0; i < 8; i++)
    {
        cout << "   ";
        for (int j = 0; j < 8; j++)
        {
            cout << "+---";
        }
        cout << "+" << endl;
        cout << i + 1 << "  ";
        for (int j = 0; j < 8; j++)
        {
            cout << "|"
                 << " " << pola[i][j] << " ";
        }
        cout << "|" << endl;
        k--;
    }
    cout << "   ";
    for (int j = 0; j < 8; j++)
    {
        cout << "+---";
    }
    cout << "+" << endl;

    cout << "     ";
    char znak = 65;
    for (int i = 0; i < 8; i++)
    {
        znak = 65 + i;
        cout << znak << "   ";
    }
}

void Plansza::Ruch(/*struct pozycja przed, struct pozycja po, struct pozycja bicie, struct pozycja krolowa*/ struct Ruch ruch)
{
    // to co bylo na wyjsciowej pozycji jest na koncowej, a pozycja poczatkowa staje sie ' '
    pola[ruch.po.x][ruch.po.y] = pola[ruch.przed.x][ruch.przed.y];
    pola[ruch.przed.x][ruch.przed.y] = ' ';

    // jesli nasz ruch to bicie to musimy usunac zbitego piona
    pola[ruch.bicie.x][ruch.bicie.y] = ' ';

    // Promocja na Krolowa
    if (kolej == 'b' && ruch.krolowa.x == 0)
    {
        pola[ruch.krolowa.x][ruch.krolowa.y] = 'BK'; // BK - Biala krolowa
    }
    if (kolej == 'c' && ruch.krolowa.x == 7)
    {
        pola[ruch.krolowa.x][ruch.krolowa.y] = 'CK'; // CK - Czarna krolowa
    }
}

bool Plansza::Czy_puste(int x, int y)
{
    return pola[x][y] == ' ';
    /*if (pola[x][y] == ' ')
    {
        return 1;
    }
    else {
        return 0;
    }*/
}

bool Plansza::Czy_przeciwnik(int x, int y)
{
    if (kolej == 'b')
    {
        return (pola[x][y] == 'c' || pola[x][y] == 'CK');
    }
    if (kolej == 'c')
    {
        return (pola[x][y] == 'b' || pola[x][y] == 'BK');
    }
}

bool Plansza::Czy_na_planszy(int x, int y)
{
    if (x < 0 || x > 7)
    {
        return false;
    }
    if (y < 0 || y > 7)
    {
        return false;
    }
    return true;
}

void Plansza::kolej_przeciwnika()
{
    if (kolej == 'b')
    {
        kolej = 'c';
    }
    else
    {
        kolej = 'b';
    }
}

struct element_listy *Ruchy::Poczatek()
{
    return posuniecia;
}

struct element_listy *Ruchy::Dodaj_posuniecie(struct pozycja przed, struct pozycja po, struct element_listy *posuniecia, Plansza *plansza)
{
    struct element_listy *nowy_element = new struct element_listy;
    nowy_element->ruch.przed = przed;
    nowy_element->ruch.po = po;
    nowy_element->ruch.bicie.x = 10;
    nowy_element->ruch.bicie.y = 10;
    nowy_element->next = posuniecia;
    posuniecia = nowy_element;
    return posuniecia;
}

struct pozycja Ruchy::pozycja(int x, int y)
{
    struct pozycja pozycja;
    pozycja.x = x;
    pozycja.y = y;
    return pozycja;
}

struct element_listy *Ruchy::Ruch_pionkiem(int x, int y, struct element_listy *posuniecia, Plansza *plansza)
{
    // 4 przypadki 2 dla bialych 2 dla czarnych
    if (plansza->kolej = 'b')
    {
        if (plansza->Czy_na_planszy(x - 1, y + 1) && plansza->Czy_puste(x - 1, y + 1))
        {
            posuniecia = Dodaj_posuniecie(pozycja(x, y), pozycja(x - 1, y + 1), posuniecia, plansza);
        }
        if (plansza->Czy_na_planszy(x + 1, y + 1) && plansza->Czy_puste(x + 1, y + 1))
        {
            posuniecia = Dodaj_posuniecie(pozycja(x, y), pozycja(x + 1, y + 1), posuniecia, plansza);
        }
    }
    else
    {
        if (plansza->Czy_na_planszy(x - 1, y - 1) && plansza->Czy_puste(x - 1, y - 1))
        {
            posuniecia = Dodaj_posuniecie(pozycja(x, y), pozycja(x - 1, y - 1), posuniecia, plansza);
        }

        if (plansza->Czy_na_planszy(x + 1, y - 1) && plansza->Czy_puste(x + 1, y - 1))
        {
            posuniecia = Dodaj_posuniecie(pozycja(x, y), pozycja(x + 1, y - 1), posuniecia, plansza);
        }
    }
    // bicia
    if (plansza->Czy_na_planszy(x - 2, y - 2) && plansza->Czy_puste(x - 2, y - 2) && plansza->Czy_przeciwnik(x - 1, y - 1))
    {
        posuniecia = Dodaj_posuniecie(pozycja(x, y), pozycja(x - 2, y - 2), posuniecia, plansza);
        posuniecia->ruch.bicie.x = x - 1;
        posuniecia->ruch.bicie.y = y - 1;
    }
    if (plansza->Czy_na_planszy(x + 2, y - 2) && plansza->Czy_puste(x + 2, y - 2) && plansza->Czy_przeciwnik(x + 1, y - 1))
    {
        posuniecia = Dodaj_posuniecie(pozycja(x, y), pozycja(x + 2, y - 2), posuniecia, plansza);
        posuniecia->ruch.bicie.x = x + 1;
        posuniecia->ruch.bicie.y = y - 1;
    }
    if (plansza->Czy_na_planszy(x - 2, y + 2) && plansza->Czy_puste(x - 2, y + 2) && plansza->Czy_przeciwnik(x - 1, y + 1))
    {
        posuniecia = Dodaj_posuniecie(pozycja(x, y), pozycja(x - 2, y + 2), posuniecia, plansza);
        posuniecia->ruch.bicie.x = x - 1;
        posuniecia->ruch.bicie.y = y + 1;
    }
    if (plansza->Czy_na_planszy(x + 2, y + 2) && plansza->Czy_puste(x + 2, y + 2) && plansza->Czy_przeciwnik(x + 1, y + 1))
    {
        posuniecia = Dodaj_posuniecie(pozycja(x, y), pozycja(x + 2, y + 2), posuniecia, plansza);
        posuniecia->ruch.bicie.x = x + 1;
        posuniecia->ruch.bicie.y = y + 1;
    }
}

struct element_listy *Ruchy::MozliwyRuch(struct pozycja poczatek, struct pozycja koniec)
{
    struct element_listy *tmp = posuniecia;

    while (tmp != NULL)
    {
        if (tmp->ruch.przed.x == poczatek.x && tmp->ruch.przed.y == poczatek.y && tmp->ruch.po.x == koniec.x && tmp->ruch.po.y == koniec.y)
        {
            return tmp;
        }
        tmp = tmp->next;
    }
    return NULL;
}
/*
struct Ruch
{
    struct pozycja przed;
    struct pozycja po;
    struct pozycja bicie;
    struct pozycja krolowa;
}; */

//////////////////////
// negamax alpha beta//
/////////////////////

int Plansza::Ocena(char gracz)
{
    char pionek, pionek_przeciwnika;
    if (gracz == 'b')
    {
        pionek = 'b';
        pionek_przeciwnika = 'c';
    }
    else
    {
        pionek = 'c';
        pionek_przeciwnika = 'b';
    }

    int punkty;
    // Moznaby to rozbudowac, np. pionki blisko siebie to wiecej pkt bo sa silniejsze niz pionki rozdzielone, aby dobrze szacowac przewage punktowa trzeba lepiej poznac strategie gry w warcaby
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            if (pola[i][j] == pionek)
            {
                punkty++;
            }
            if (pola[i][j] == pionek_przeciwnika)
            {
                punkty--;
            }
        }
    }
    return punkty;
}

int negamax(Plansza *plansza, struct Ruch *ruchh, int alfa, int beta, char gracz, int poziom)
{
    if (poziom == 0)
    {
        int ocena = plansza->Ocena(gracz);
        return ocena;
    }

    Ruchy ruchy(plansza);
    struct element_listy *posuniecia = ruchy.Poczatek();

    int stop = 1;
    while (posuniecia != NULL && stop)
    {
        struct Ruch ruch = posuniecia->ruch;
        posuniecia = posuniecia->next;

        // musimy zapisac ten ruch zeby potem moc go cofnac?
        char poczatek = plansza->pola[ruch.przed.x][ruch.przed.y];
        char koniec = plansza->pola[ruch.po.x][ruch.po.y];
        char bicie;
        if (ruch.bicie.x > -1 && ruch.bicie.x < 8 && ruch.bicie.y > -1 && ruch.bicie.y < 8)
        {
            bicie = plansza->pola[ruch.bicie.x][ruch.bicie.y];
        }

        plansza->Ruch(ruch);
        plansza->kolej_przeciwnika();
        char kolejka = plansza->kolej;

        // kolej gracza
        if (gracz == kolejka)
        {
            int wartosc;
            wartosc = negamax(plansza, ruchh, alfa, beta, gracz, poziom - 1);
            if (alfa <= wartosc)
            {
                alfa = wartosc;
                (*ruchh) = ruch;
            }
            if (alfa >= beta)
            {
                stop = 0;
            }
        }
        else
        {
            int wartosc;
            wartosc = negamax(plansza, ruchh, alfa, beta, gracz, poziom - 1);
            if (beta >= wartosc)
            {
                beta = wartosc;
            }
            if (alfa >= beta)
            {
                stop = 0;
            }
        }

        // cofamy ten ruch
        plansza->pola[ruch.przed.x][ruch.przed.y] = poczatek;
        plansza->pola[ruch.po.x][ruch.po.y] = koniec;
        if (ruch.bicie.x > -1 && ruch.bicie.x < 8 && ruch.bicie.y > -1 && ruch.bicie.y < 8)
        {
            plansza->pola[ruch.bicie.x][ruch.bicie.y] = bicie;
        }

        plansza->kolej = kolejka;
    }
    if (gracz == plansza->kolej)
    {
        return alfa;
    }
    else
    {
        return beta;
    }
}

void Ruch_komputera(Plansza *plansza)
{
    cout << "Komputer wykonuje swoj ruch!" << endl;
    struct Ruch ruch;
    negamax(plansza, &ruch, -1000, 1000, plansza->kolej, 2);
    plansza->Ruch(ruch);
}

struct pozycja pozycja(int x, int y)
{
    struct pozycja pozycja;
    pozycja.x = x;
    pozycja.y = y;
    return pozycja;
}

void Ruch_gracza(Plansza *plansza)
{
    struct element_listy *element = NULL;
    Ruchy posuniecia(plansza);

    int wiersz, kolumna;

    while (element == NULL)
    {
        cout << "Teraz Twoj ruch!" << endl;
        cout << "Podaj wspolrzedne pionka, ktorym chcesz sie ruszyc" << endl;
        cout << "Wiersz: ";
        cin >> wiersz;
        cout << endl;
        cout << "Kolumna: ";
        cin >> kolumna;
        cout << endl;
        struct pozycja poczatek = pozycja(wiersz - 1, kolumna - 1);
        cout << "Podaj wspolrzedne pola, na ktore ma przejsc pionek" << endl;
        cout << "Wiersz: ";
        cin >> wiersz;
        cout << endl;
        cout << "Kolumna: ";
        cin >> kolumna;
        cout << endl;
        struct pozycja koniec = pozycja(wiersz - 1, kolumna - 1);

        element = posuniecia.MozliwyRuch(poczatek, koniec);

        if (element == NULL)
        {
            cout << "Nie mozna wykonac takiego ruchu" << endl;
            cout << "Sprobuj jeszcze raz :)" << endl;
            plansza->Wyswietl_plansze();
        }
        else
        {
            plansza->Ruch(element->ruch);
        }
    }
}

void Gra(Plansza *plansza, char wybor)
{
    if (plansza->kolej == wybor)
    {
        Ruch_gracza(plansza);
    }
    else
    {
        Ruch_komputera(plansza);
    }
    plansza->Wyswietl_plansze();

    plansza->kolej_przeciwnika();
}