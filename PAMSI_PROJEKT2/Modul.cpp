#include "Modul.h"
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <algorithm>
#include <vector>
#include <cstddef>
#include <fstream>

using namespace std;

/* Wczytanie danych z excela do dokumentu tekstowego (txt), filtruje i usuwa filmy bez oceny*/
void Ranking_filmow::wczytaj_do_txt()
{
    string tmp;
    ifstream plik("projekt2_dane.csv");
    ofstream nowy_plik("projekt2_dane.txt");
    int licznik;

    while (getline(plik, tmp))
    {
        if (tmp[tmp.length() - 1] == '0') // Kazda linia konczaca sie ocena i ostatnim znakiem jest 0, np. 5.0 lub 10.0
        {
            nowy_plik << tmp << endl;
            licznik++;
        }
    }
    nowy_plik.close();
    plik.close();
    cout << "Po przefiltrowaniu zostalo " << licznik << " filmow" << endl;
}

/* Funkcja dodaje film do tablicy */
void Ranking_filmow::dodaj_film(string nazwa_filmu, float ocena_filmu, int miejsce, Ranking_filmow *tab)
{
    tab[miejsce].nazwa_filmu = nazwa_filmu;
    tab[miejsce].ocena_filmu = ocena_filmu;
}
/* Funkcja, ktora dodaje do tablicy wskazana liczbe danych */
Ranking_filmow *Ranking_filmow::odczytaj_dane(int rozmiar)
{
    string tmp;
    ifstream nowy_plik("projekt2_dane.txt");
    Ranking_filmow *tab = new Ranking_filmow[rozmiar];

    if (!nowy_plik.is_open()) // sprawdzenie
    {
        cout << "Plik nie istnieje!" << endl;
        delete[] tab;
        exit(0);
    }
    else
    {
        for (int i = 0; i < rozmiar; i++)
        {
            char znaki[5];
            getline(nowy_plik, tmp);    // wczytujemy linie i zapisujemy do tmp
            int dlugosc = tmp.length(); // dlugosc to liczba znakow linii

            tmp.copy(znaki, 4, dlugosc - 4); // kopiujemy 4 ostatnie znaki czyli przecinek i ocene lub cala ocene (,9.0 lub ,3.0 lub 10.0)
            znaki[4] = '\0';                 // ustawiamy ostatni znak jako koniec ciagu znakow
            tmp.erase(dlugosc - 4, 4);       // usuwamy 4 ostatnie znaki ze stringa czyli ocene i mamy nazwe

            if (znaki[0] == ',') // przypadek gdy ocena jest od 1 do 9
            {
                char znaki_tmp[4];
                for (int i = 0; i < 3; i++)
                {
                    znaki_tmp[i] = znaki[i + 1]; // pozbywamy sie przecinka
                }
                znaki_tmp[4] = '\0';
                float ocena = atof(znaki_tmp); // konwertujemy char do float
                dodaj_film(tmp, ocena, i, tab);
            }
            else // przypadek dla oceny 10.0
            {
                float ocena = atof(znaki);
                dodaj_film(tmp, ocena, i, tab);
            }
        }
        return tab;
    }
}
/* Funkcja zwraca srednia wartosc wszystkich ocen w tablicy */
float Ranking_filmow::srednia_wartosc(Ranking_filmow *tab, int rozmiar)
{
    float tmp = 0, srednia_wartosc = 0;
    for (int i = 0; i < rozmiar; i++)
    {
        tmp += tab[i].ocena_filmu;
    }
    srednia_wartosc = tmp / rozmiar;
    return srednia_wartosc;
}
/* Funkcja zwraca mediane, czyli wartosc srodkowa */
float Ranking_filmow::mediana(Ranking_filmow *tab, int rozmiar)
{
    float mediana = 0;
    if (rozmiar % 2 == 0) // dla parzystej liczby danych mediana jest srednia dwoch srodkowych elementow
    {
        mediana = (tab[rozmiar / 2].ocena_filmu + tab[rozmiar / 2 + 1].ocena_filmu) / 2;
    }
    else // dal nieparzystej liczby danych mediana jest po prostu elementem srodkowym
    {
        mediana = tab[rozmiar / 2 + 1].ocena_filmu;
    }
    return mediana;
}
/* Sortowanie merge */
void Ranking_filmow::merge(Ranking_filmow *tab, int lewa, int srodek, int prawa)
{
    const int rozmiar_lewa = srodek - lewa + 1;
    const int rozmiar_prawa = prawa - srodek;

    Ranking_filmow *tab1 = new Ranking_filmow[rozmiar_lewa];
    Ranking_filmow *tab2 = new Ranking_filmow[rozmiar_prawa];

    for (int i = 0; i < rozmiar_lewa; i++)
    {
        tab1[i].nazwa_filmu = tab[lewa + i].nazwa_filmu;
        tab1[i].ocena_filmu = tab[lewa + i].ocena_filmu;
    }
    for (int i = 0; i < rozmiar_prawa; i++)
    {
        tab2[i].nazwa_filmu = tab[srodek + 1 + i].nazwa_filmu;
        tab2[i].ocena_filmu = tab[srodek + 1 + i].ocena_filmu;
    }

    int licznik_lewa = 0;
    int licznik_prawa = 0;
    int licznik = lewa;

    while (licznik_lewa < rozmiar_lewa && licznik_prawa < rozmiar_prawa)
    {
        if (tab1[licznik_lewa].ocena_filmu < tab2[licznik_prawa].ocena_filmu)
        {
            tab[licznik].nazwa_filmu = tab1[licznik_lewa].nazwa_filmu;
            tab[licznik].ocena_filmu = tab1[licznik_lewa].ocena_filmu;
            licznik_lewa++;
            licznik++;
        }
        else
        {
            tab[licznik].nazwa_filmu = tab2[licznik_prawa].nazwa_filmu;
            tab[licznik].ocena_filmu = tab2[licznik_prawa].ocena_filmu;
            licznik_prawa++;
            licznik++;
        }
    }

    while (licznik_lewa < rozmiar_lewa)
    {
        tab[licznik].nazwa_filmu = tab1[licznik_lewa].nazwa_filmu;
        tab[licznik].ocena_filmu = tab1[licznik_lewa].ocena_filmu;
        licznik_lewa++;
        licznik++;
    }

    while (licznik_prawa < rozmiar_prawa)
    {
        tab[licznik].nazwa_filmu = tab2[licznik_prawa].nazwa_filmu;
        tab[licznik].ocena_filmu = tab2[licznik_prawa].ocena_filmu;
        licznik_prawa++;
        licznik++;
    }

    delete[] tab1;
    delete[] tab2;
}

void Ranking_filmow::mergesort(Ranking_filmow *tab, int lewa, int prawa)
{
    if (lewa < prawa)
    {
        int srodek = (lewa + prawa) / 2;
        mergesort(tab, lewa, srodek);
        mergesort(tab, srodek + 1, prawa);

        merge(tab, lewa, srodek, prawa);
    }
}

void Ranking_filmow::swap(Ranking_filmow *tab, int i, int j)
{
    string tmp_nazwa = tab[i].nazwa_filmu;
    float tmp_ocena = tab[i].ocena_filmu;

    tab[i].nazwa_filmu = tab[j].nazwa_filmu;
    tab[i].ocena_filmu = tab[j].ocena_filmu;

    tab[j].nazwa_filmu = tmp_nazwa;
    tab[j].ocena_filmu = tmp_ocena;
}

int Ranking_filmow::partition(Ranking_filmow *tab, int lewa, int prawa)
{
    float pivot = tab[(lewa + prawa) / 2].ocena_filmu;
    int i = lewa;
    int j = prawa;

    while (1)
    {
        while (tab[j].ocena_filmu > pivot)
        {
            j--;
        }
        while (tab[i].ocena_filmu < pivot)
        {
            i++;
        }

        if (i < j)
        {
            swap(tab, i++, j--);
        }
        else
        {
            return j;
        }
    }
}
/* Sortowanie quicksort */
void Ranking_filmow::quicksort(Ranking_filmow *tab, int lewa, int prawa)
{
    if (lewa < prawa)
    {
        int pi = partition(tab, lewa, prawa);
        quicksort(tab, lewa, pi);
        quicksort(tab, pi + 1, prawa);
    }
}
/* Sortowanie bucketsort */
void Ranking_filmow::bucketsort(Ranking_filmow *tab, int rozmiar)
{
    int licznik1 = 0, licznik2 = 0, licznik3 = 0, licznik4 = 0, licznik5 = 0;

    for (int i = 0; i < rozmiar; i++)
    {
        if (tab[i].ocena_filmu == 1 || tab[i].ocena_filmu == 2)
        {
            licznik1++;
        }
        if (tab[i].ocena_filmu == 3 || tab[i].ocena_filmu == 4)
        {
            licznik2++;
        }
        if (tab[i].ocena_filmu == 5 || tab[i].ocena_filmu == 6)
        {
            licznik3++;
        }
        if (tab[i].ocena_filmu == 7 || tab[i].ocena_filmu == 8)
        {
            licznik4++;
        }
        if (tab[i].ocena_filmu == 9 || tab[i].ocena_filmu == 10)
        {
            licznik5++;
        }
    }

    Ranking_filmow **wiadro = new Ranking_filmow *[5];

    wiadro[0] = new Ranking_filmow[licznik1];
    wiadro[1] = new Ranking_filmow[licznik2];
    wiadro[2] = new Ranking_filmow[licznik3];
    wiadro[3] = new Ranking_filmow[licznik4];
    wiadro[4] = new Ranking_filmow[licznik5];

    licznik1 = 0;
    licznik2 = 0;
    licznik3 = 0;
    licznik4 = 0;
    licznik5 = 0;

    for (int i = 0; i < rozmiar; i++)
    {
        if (tab[i].ocena_filmu == 1 || tab[i].ocena_filmu == 2)
        {
            wiadro[0][licznik1].nazwa_filmu = tab[i].nazwa_filmu;
            wiadro[0][licznik1].ocena_filmu = tab[i].ocena_filmu;
            licznik1++;
        }
        if (tab[i].ocena_filmu == 3 || tab[i].ocena_filmu == 4)
        {
            wiadro[1][licznik2].nazwa_filmu = tab[i].nazwa_filmu;
            wiadro[1][licznik2].ocena_filmu = tab[i].ocena_filmu;
            licznik2++;
        }
        if (tab[i].ocena_filmu == 5 || tab[i].ocena_filmu == 6)
        {
            wiadro[2][licznik3].nazwa_filmu = tab[i].nazwa_filmu;
            wiadro[2][licznik3].ocena_filmu = tab[i].ocena_filmu;
            licznik3++;
        }
        if (tab[i].ocena_filmu == 7 || tab[i].ocena_filmu == 8)
        {
            wiadro[3][licznik4].nazwa_filmu = tab[i].nazwa_filmu;
            wiadro[3][licznik4].ocena_filmu = tab[i].ocena_filmu;
            licznik4++;
        }
        if (tab[i].ocena_filmu == 9 || tab[i].ocena_filmu == 10)
        {
            wiadro[4][licznik5].nazwa_filmu = tab[i].nazwa_filmu;
            wiadro[4][licznik5].ocena_filmu = tab[i].ocena_filmu;
            licznik5++;
        }
    }

    quicksort(wiadro[0], 0, licznik1 - 1);
    quicksort(wiadro[1], 0, licznik2 - 1);
    quicksort(wiadro[2], 0, licznik3 - 1);
    quicksort(wiadro[3], 0, licznik4 - 1);
    quicksort(wiadro[4], 0, licznik5 - 1);

    for (int i = 0; i < rozmiar; i++)
    {
        int j;
        while (licznik1 > 0)
        {
            tab[i].nazwa_filmu = wiadro[0][j].nazwa_filmu;
            tab[i].ocena_filmu = wiadro[0][j].ocena_filmu;
            licznik1--;
            i++;
            j++;
        }
        j = 0;
        while (licznik2 > 0)
        {
            tab[i].nazwa_filmu = wiadro[1][j].nazwa_filmu;
            tab[i].ocena_filmu = wiadro[1][j].ocena_filmu;
            licznik2--;
            i++;
            j++;
        }
        j = 0;
        while (licznik3 > 0)
        {
            tab[i].nazwa_filmu = wiadro[2][j].nazwa_filmu;
            tab[i].ocena_filmu = wiadro[2][j].ocena_filmu;
            licznik3--;
            i++;
            j++;
        }
        j = 0;
        while (licznik4 > 0)
        {
            tab[i].nazwa_filmu = wiadro[3][j].nazwa_filmu;
            tab[i].ocena_filmu = wiadro[3][j].ocena_filmu;
            licznik4--;
            i++;
            j++;
        }
        j = 0;
        while (licznik5 > 0)
        {
            tab[i].nazwa_filmu = wiadro[4][j].nazwa_filmu;
            tab[i].ocena_filmu = wiadro[4][j].ocena_filmu;
            licznik5--;
            i++;
            j++;
        }
    }
    delete[] wiadro;
    delete[] wiadro[0];
    delete[] wiadro[1];
    delete[] wiadro[2];
    delete[] wiadro[3];
    delete[] wiadro[4];
}
/* Funkcja sprawdza czy sortowanie sie udalo, sprawdza czy kolejny element jest mniejszy od poprzedniego - wtedy jest blad */
void Ranking_filmow::sprawdz_sortowanie(Ranking_filmow *tab, int rozmiar)
{
    for (int i = 0; i < rozmiar - 1; i++)
    {
        if (tab[i].ocena_filmu > tab[i + 1].ocena_filmu)
        {
            cout << "Liczby nie sa posortowane, wystapil blad w sortowaniu" << endl;
        }
    }
}