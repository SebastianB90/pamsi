#include "Modul.h"
#include "Modul.cpp"
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <algorithm>
#include <vector>
#include <cstddef>
#include <fstream>

using namespace std;

int main()
{
    Ranking_filmow *tab = new Ranking_filmow;
    Ranking_filmow program;
    int wybor;

    program.wczytaj_do_txt();
    // Po przefiltrowaniu zostało 962903 filmow, zatem usunalem case dla miliona filmow do posortowania, poniewaz maksymalna ilosc to 962 903.
    while (1)
    {
        /* To jest menu, uzytkownik dokonuje wyboru ile danych chce posortowac oraz jakim algorytmem.
        Kolejne case'y są analogiczne, roznia sie nieznacznie (liczba danych, funckja z danym algorytmem sortowania) */
        cout << endl;
        cout << "Program do sortowania filmow wg ocen" << endl;
        cout << "Wybierz ile filmow chcesz posortowac" << endl;
        cout << "1. 10 000" << endl;
        cout << "2. 100 000" << endl;
        cout << "3. 500 000" << endl;
        cout << "4. Maksymalna liczba filmow (962 903)" << endl;
        cout << "5. Zakoncz program" << endl;
        cin >> wybor;

        switch (wybor)
        {
        case 1:
        {
            int wybor2;
            tab = program.odczytaj_dane(10000);

            cout << endl;
            cout << "Wybierz jakiego sortowania chcesz uzyc" << endl;
            cout << "1. MergeSort" << endl;
            cout << "2. QuickSort" << endl;
            cout << "3. BucketSort" << endl;
            cin >> wybor2;

            switch (wybor2)
            {
            case 1:
            {
                double czas_start = (double)clock();                       // Wlaczamy zegar zeby zmierzyc czas sortowania
                program.mergesort(tab, 0, 9999);                           // wykonujemy sortowanie dla danej liczby danych
                double czas_stop = (double)clock();                        // zegar stop
                program.sprawdz_sortowanie(tab, 10000);                    // sprawdzamy czy liczby sa dobrze posortowane
                double czas = (czas_stop - czas_start) / (CLOCKS_PER_SEC); // obliczamy czas sortowania
                cout << "Czas sortowania: " << czas << "s." << endl;
                cout << "Srednia wartosc: " << program.srednia_wartosc(tab, 10000) << endl; // obliczamy srednia wartosc ocen
                cout << "Mediana: " << program.mediana(tab, 10000) << endl;                 // obliczamy mediane ocen
                /*for(int i=0; i< 10000; i++)
                {
                    cout << tab[i].ocena_filmu << " ";
                }*/
                delete[] tab;
                break;
            }
                /* KOMENTARZE DLA RESZTY CASE'OW SA ANALOGICZNE */
            case 2:
            {
                double czas_start = (double)clock();
                program.quicksort(tab, 0, 9999);
                double czas_stop = (double)clock();
                program.sprawdz_sortowanie(tab, 10000);
                double czas = (czas_stop - czas_start) / (CLOCKS_PER_SEC);
                cout << "Czas sortowania: " << czas << "s." << endl;
                cout << "Srednia wartosc: " << program.srednia_wartosc(tab, 10000) << endl;
                cout << "Mediana: " << program.mediana(tab, 10000) << endl;
                delete[] tab;
                break;
            }

            case 3:
            {
                double czas_start = (double)clock();
                program.bucketsort(tab, 10000);
                double czas_stop = (double)clock();
                program.sprawdz_sortowanie(tab, 10000);
                double czas = (czas_stop - czas_start) / (CLOCKS_PER_SEC);
                cout << "Czas sortowania: " << czas << "s." << endl;
                cout << "Srednia wartosc: " << program.srednia_wartosc(tab, 10000) << endl;
                cout << "Mediana: " << program.mediana(tab, 10000) << endl;
                delete[] tab;
                break;
            }
            }
            break;
        }

        case 2:
        {
            int wybor3;
            tab = program.odczytaj_dane(100000);

            cout << endl;
            cout << "Wybierz jakiego sortowania chcesz uzyc" << endl;
            cout << "1. MergeSort" << endl;
            cout << "2. QuickSort" << endl;
            cout << "3. BucketSort" << endl;
            cin >> wybor3;

            switch (wybor3)
            {
            case 1:
            {
                double czas_start = (double)clock();
                program.mergesort(tab, 0, 99999);
                double czas_stop = (double)clock();
                program.sprawdz_sortowanie(tab, 100000);
                double czas = (czas_stop - czas_start) / (CLOCKS_PER_SEC);
                cout << "Czas sortowania: " << czas << "s." << endl;
                cout << "Srednia wartosc: " << program.srednia_wartosc(tab, 100000) << endl;
                cout << "Mediana: " << program.mediana(tab, 100000) << endl;
                delete[] tab;
                break;
            }

            case 2:
            {
                double czas_start = (double)clock();
                program.quicksort(tab, 0, 99999);
                double czas_stop = (double)clock();
                program.sprawdz_sortowanie(tab, 100000);
                double czas = (czas_stop - czas_start) / (CLOCKS_PER_SEC);
                cout << "Czas sortowania: " << czas << "s." << endl;
                cout << "Srednia wartosc: " << program.srednia_wartosc(tab, 100000) << endl;
                cout << "Mediana: " << program.mediana(tab, 100000) << endl;
                delete[] tab;
                break;
            }

            case 3:
            {
                double czas_start = (double)clock();
                program.bucketsort(tab, 100000);
                double czas_stop = (double)clock();
                program.sprawdz_sortowanie(tab, 100000);
                double czas = (czas_stop - czas_start) / (CLOCKS_PER_SEC);
                cout << "Czas sortowania: " << czas << "s." << endl;
                cout << "Srednia wartosc: " << program.srednia_wartosc(tab, 100000) << endl;
                cout << "Mediana: " << program.mediana(tab, 100000) << endl;
                delete[] tab;
                break;
            }
            }
            break;
        }

        case 3:
        {
            int wybor4;
            tab = program.odczytaj_dane(500000);

            cout << endl;
            cout << "Wybierz jakiego sortowania chcesz uzyc" << endl;
            cout << "1. MergeSort" << endl;
            cout << "2. QuickSort" << endl;
            cout << "3. BucketSort" << endl;
            cin >> wybor4;

            switch (wybor4)
            {
            case 1:
            {
                double czas_start = (double)clock();
                program.mergesort(tab, 0, 499999);
                double czas_stop = (double)clock();
                program.sprawdz_sortowanie(tab, 500000);
                double czas = (czas_stop - czas_start) / (CLOCKS_PER_SEC);
                cout << "Czas sortowania: " << czas << "s." << endl;
                cout << "Srednia wartosc: " << program.srednia_wartosc(tab, 500000) << endl;
                cout << "Mediana: " << program.mediana(tab, 500000) << endl;
                delete[] tab;
                break;
            }

            case 2:
            {
                double czas_start = (double)clock();
                program.quicksort(tab, 0, 499999);
                double czas_stop = (double)clock();
                program.sprawdz_sortowanie(tab, 500000);
                double czas = (czas_stop - czas_start) / (CLOCKS_PER_SEC);
                cout << "Czas sortowania: " << czas << "s." << endl;
                cout << "Srednia wartosc: " << program.srednia_wartosc(tab, 500000) << endl;
                cout << "Mediana: " << program.mediana(tab, 500000) << endl;
                delete[] tab;
                break;
            }

            case 3:
            {
                double czas_start = (double)clock();
                program.bucketsort(tab, 500000);
                double czas_stop = (double)clock();
                program.sprawdz_sortowanie(tab, 500000);
                double czas = (czas_stop - czas_start) / (CLOCKS_PER_SEC);
                cout << "Czas sortowania: " << czas << "s." << endl;
                cout << "Srednia wartosc: " << program.srednia_wartosc(tab, 500000) << endl;
                cout << "Mediana: " << program.mediana(tab, 500000) << endl;
                delete[] tab;
                break;
            }
            }
            break;
        }

        case 4:
        {
            int wybor5;
            tab = program.odczytaj_dane(962903);

            cout << endl;
            cout << "Wybierz jakiego sortowania chcesz uzyc" << endl;
            cout << "1. MergeSort" << endl;
            cout << "2. QuickSort" << endl;
            cout << "3. BucketSort" << endl;
            cin >> wybor5;

            switch (wybor5)
            {
            case 1:
            {
                double czas_start = (double)clock();
                program.mergesort(tab, 0, 962902);
                double czas_stop = (double)clock();
                program.sprawdz_sortowanie(tab, 962903);
                double czas = (czas_stop - czas_start) / (CLOCKS_PER_SEC);
                cout << "Czas sortowania: " << czas << "s." << endl;
                cout << "Srednia wartosc: " << program.srednia_wartosc(tab, 962903) << endl;
                cout << "Mediana: " << program.mediana(tab, 962903) << endl;
                delete[] tab;
                break;
            }

            case 2:
            {
                double czas_start = (double)clock();
                program.quicksort(tab, 0, 962902);
                double czas_stop = (double)clock();
                program.sprawdz_sortowanie(tab, 962903);
                double czas = (czas_stop - czas_start) / (CLOCKS_PER_SEC);
                cout << "Czas sortowania: " << czas << "s." << endl;
                cout << "Srednia wartosc: " << program.srednia_wartosc(tab, 962903) << endl;
                cout << "Mediana: " << program.mediana(tab, 962903) << endl;
                delete[] tab;
                break;
            }

            case 3:
            {
                double czas_start = (double)clock();
                program.bucketsort(tab, 962903);
                double czas_stop = (double)clock();
                program.sprawdz_sortowanie(tab, 962903);
                double czas = (czas_stop - czas_start) / (CLOCKS_PER_SEC);
                cout << "Czas sortowania: " << czas << "s." << endl;
                cout << "Srednia wartosc: " << program.srednia_wartosc(tab, 962903) << endl;
                cout << "Mediana: " << program.mediana(tab, 962903) << endl;
                delete[] tab;
                break;
            }
            }
            break;
        }

        case 5:
        {
            cout << "Koniec dzialania programu" << endl;
            exit(0);
            break;
        }

        default:
            break;
        }
    }

    return 0;
}