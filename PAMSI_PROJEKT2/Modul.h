#ifndef MODUL_H
#define MODUL_H

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <algorithm>
#include <vector>
#include <cstddef>
#include <fstream>

using namespace std;

class Ranking_filmow
{
public:
    string nazwa_filmu;
    float ocena_filmu;

    void wczytaj_do_txt();
    void dodaj_film(string nazwa_filmu, float ocena_filmu, int miejsce, Ranking_filmow *tab);
    Ranking_filmow *odczytaj_dane(int rozmiar);

    float srednia_wartosc(Ranking_filmow *tab, int rozmiar);
    float mediana(Ranking_filmow *tab, int rozmiar);

    void merge(Ranking_filmow *tab, int lewa, int srodek, int prawa);
    void mergesort(Ranking_filmow *tab, int lewa, int prawa);
    void swap(Ranking_filmow *tab, int i, int j);
    int partition(Ranking_filmow *tab, int lewa, int prawa);
    void quicksort(Ranking_filmow *tab, int lewa, int prawa);
    void bucketsort(Ranking_filmow *tab, int rozmiar);

    void sprawdz_sortowanie(Ranking_filmow *tab, int rozmiar);
};

#endif