#ifndef MODUL_HH
#define MODUL_HH

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <vector>
#include <queue>

using namespace std;

/*****************************************************
 * To jest modul. Zawiera strukture wiadomosci oraz  *
 * klase kolejki priorytetowej wraz z metodami tj.   *
 * konstruktor, destruktor, insert, removeMin itd... *                  
******************************************************/

struct message
{
    int key;
    string text;
    message *next;
};

class Priority_queue
{
private:
    message *head; //wskaźnik na początek kolejeczki
public:
    Priority_queue();
    ~Priority_queue();
    void insert(int k, string x);
    void removeMin();
    message remove(int k);
    string min();
    void print();
    int size();
    bool isEmpty();
    void merge(string &W);
};

/* Konstruktor */
Priority_queue::Priority_queue()
    : head(NULL) {}

/* Destruktor */
Priority_queue::~Priority_queue()
{
    while (!isEmpty())
    {
        removeMin();
        delete head;
    }
}

/* Insert - dodaje element o kluczu k i wartości x */
void Priority_queue::insert(int k, string x)
{
    message *W = new message;
    message *tmp = head;

    W->key = k;
    W->text = x;
    W->next = NULL;

    if (head == NULL)
    {
        head = W;
    }
    else if (k < head->key)
    {
        W->next = head;
        head = W;
    }
    else
    {
        while (tmp->next != NULL && k > tmp->next->key)
        {
            tmp = tmp->next;
        }
        W->next = tmp->next;
        tmp->next = W;
    }
}

/* removeMin - usuwa element o najmniejszym kluczu, czyli największym prio */
void Priority_queue::removeMin()
{
    message *tmp = head;
    head = tmp->next;
    delete tmp;
}

/* remove - usuwa konkretny element o danym kluczu */ /* i zwraca ten element (dodane potem, zeby wylosowac kolejnosc pakietow u anny*/
message Priority_queue::remove(int k)
{
    message *tmp = head;
    message *W = new message;
    int i = 1;
    if (k == 0)
    {
        W->text = tmp->text;
        W->key = tmp->key;
        removeMin();
    }
    else
    {
        while (tmp->next != NULL && i != k)
        {
            tmp = tmp->next;
            i++;
        }
        W->text = tmp->next->text;
        W->key = tmp->next->key;
        message *out = new message;
        out = tmp->next;
        if (tmp->next->next != NULL)
        {
            tmp->next = tmp->next->next;
        }
        if (tmp->next->next == NULL)
        {
            tmp->next = NULL;
        }
        delete out;
    }
    return *W;
}

/* min - zwraca element o najmniejszym kluczu */
string Priority_queue::min()
{
    return head->text;
    // cout << "Element o najmniejszym kluczu to: " << head -> text << endl;
}

/* print - wyswietla zawartość kolejki */
void Priority_queue::print()
{
    message *tmp = head;
    while (tmp != NULL)
    {
        cout << tmp->text;
        tmp = tmp->next;
    }
    cout << endl;
}

/* size - zwraca rozmiar kolejki */
int Priority_queue::size()
{
    int counter = 0;
    message *tmp = head;
    while (tmp != NULL)
    {
        tmp = tmp->next;
        counter++;
    }
    return counter;
}

/* isEmpty - zwraca 1 jeśli kolejka jest pusta (head jest NULLEM), a 0 jeśli kolejka nie jest pusta (head nie jest NULLEM) */
bool Priority_queue::isEmpty()
{
    return (head == NULL);
}

/* merge - potrzebne aby polaczyc pakiety ktore dostala Anna w losowej kolejnosci w jednego stringa */
void Priority_queue::merge(string &W)
{
    message *tmp = head;
    while (tmp != NULL)
    {
        W += tmp->text;
        tmp = tmp->next;
    }
}

#endif