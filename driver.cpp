#include "Modul.hh"

using namespace std;

/*************************************************************
 * To jest driver, czyli program który ma pokazać działanie  *
 * implementowanych struktur danych i metod z pliku Modul.hh *                 
**************************************************************/

int main()
{

    string planety[] = {"m", "W", "Z", "M", "J", "S", "U", "N"};
    Priority_queue Kolejka;

    cout << "Tworzymy kolejke priorytetowa" << endl;

    for (int i = 0; i < 8; i++)
    {
        Kolejka.insert(i, planety[i]);
    }

    cout << "Wyswietlimy zawartosc kolejki: ";
    Kolejka.print();

    cout << "Usuniemy element z najnizszym kluczem, czyli najwiekszym priorytetem" << endl;
    cout << "W tym przypadku bedzie to merkury (m)" << endl;
    Kolejka.removeMin();
    Kolejka.print();
    cout << "Powtorzymy ten krok. Teraz najwiekszy priorytet ma Wenus (W)" << endl;
    Kolejka.removeMin();
    Kolejka.print();
    cout << "Teraz usuniemy element o kluczu 3, powinien to byc Saturn (S)" << endl;
    Kolejka.remove(3);
    Kolejka.print();
    cout << "Zwrocimy element o najwiekszym priorytecie. Powinnna to byc Ziemia (Z)" << endl;
    cout << Kolejka.min() << endl;
    cout << "Teraz sprawdzimy rozmiar naszej kolejki. Mielismy 8 planet ukladu slonecznego, usunelismy 3, zatem rozmiar kolejki powinien byc 5" << endl;
    cout << Kolejka.size() << endl;
    cout << "Sprawdzimy czy kolejka jest pusta; 1 - tak, 0 - nie" << endl;
    cout << Kolejka.isEmpty() << endl;
    cout << "Teraz usuniemy pozostale 5 planet i sprawdzimy czy kolejka bedzie pusta" << endl;

    for (int i = 0; i < 5; i++)
    {
        Kolejka.removeMin();
    }
    cout << Kolejka.isEmpty() << endl;

    return 0;
}